#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
struct Node *search(struct Node *head, int searchValue);
void deleteAll(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
    struct Node *head = NULL;
    int choice, data, searchValue, newValue, key, value;

    while (1)
    {
        printf("\nLinked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Search\n");
        printf("6. Delete All\n");
        printf("7. Insert After Key\n");
        printf("8. Insert After Value\n");
        printf("9. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            printList(head);
            break;
        case 2:
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
            break;
        case 3:
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
            break;
        case 4:
            printf("Enter key to delete by key: ");
            scanf("%d", &key);
            deleteByKey(&head, key);
            break;
        case 5:
            printf("Enter value to search: ");
            scanf("%d", &searchValue);
            struct Node *found = search(head, searchValue);
            if (found != NULL)
            {
                printf("Found node with value %d and key %d\n", found->number, found - head);
            }
            else
            {
                printf("Node not found\n");
            }
            break;
        case 6:
            printf("Enter value to delete all: ");
            scanf("%d", &value);
            deleteAll(&head, value);
            break;
        case 7:
            printf("Enter key to insert after: ");
            scanf("%d", &key);
            printf("Enter new value: ");
            scanf("%d", &newValue);
            insertAfterKey(&head, key, newValue);
            break;
        case 8:
            printf("Enter value to insert after: ");
            scanf("%d", &searchValue);
            printf("Enter new value: ");
            scanf("%d", &newValue);
            insertAfterValue(&head, searchValue, newValue);
            break;
        case 9:
            return 0;
        default:
            printf("Invalid choice. Please try again.\n");
            break;
        }
    }

    return 0;
}
// creates a new node with the given integer value and returns a pointer to the newly created code
struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode == NULL)
    {
        printf("Memory allocation failed\n");
        exit(1);
    }
    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}
// prints the elements of the linked list, from the head node. if empty prints empty list
void printList(struct Node *head)
{
    if (head == NULL)
    {
        printf("List is empty\n");
        return;
    }

    struct Node *current = head;
    int index = 0;
    while (current != NULL)
    {
        printf("Node %d: %d\n", index, current->number);
        current = current->next;
        index++;
    }
}
// adds a new node  at the end of the linked list. If no list exists, it will create one
void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);

    if (*head == NULL)
    {
        *head = newNode;
        return;
    }

    struct Node *current = *head;
    while (current->next != NULL)
    {
        current = current->next;
    }
    current->next = newNode;
}
// adds a new node with the given integer value to the beginning  of the linked list
void prepend(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}
// deletes the first node with the given key value from the linked list
void deleteByKey(struct Node **head, int key)
{
    if (*head == NULL)
    {
        printf("List is empty\n");
        return;
    }

    if ((*head)->number == key)
    {
        struct Node *temp = *head;
        *head = (*head)->next;
        free(temp);
        return;
    }

    struct Node *current = *head;
    while (current->next != NULL & current->next->number != key)
    {
        current = current->next;
    }

    if (current->next == NULL)
    {
        printf("Key not found\n");
        return;
    }

    struct Node *temp = current->next;
    current->next = current->next->next;
    free(temp);
}
// deletes the first node with the given value from the linked list
void deleteByValue(struct Node **head, int value)
{
    if (*head == NULL)
    {
        printf("List is empty\n");
        return;
    }

    if ((*head)->number == value)
    {
        struct Node *temp = *head;
        *head = (*head)->next;
        free(temp);
        return;
    }

    struct Node *current = *head;
    struct Node *previous = NULL;
    while (current != NULL && current->number != value)
    {
        previous = current;
        current = current->next;
    }

    if (current == NULL)
    {
        printf("Value not found\n");
        return;
    }

    previous->next = current->next;
    free(current);
}
// returns a pointer to the first node with the given value in the linked list, or NULL if not found
struct Node *search(struct Node *head, int searchValue)
{
    struct Node *current = head;
    int index = 0;
    while (current != NULL)
    {
        if (current->number == searchValue)
        {
            return current;
        }
        current = current->next;
        index++;
    }
    return NULL;
}
// deletes all nodes with the given value from the linked list
void deleteAll(struct Node **head, int value)
{
    if (*head == NULL)
    {
        printf("List is empty\n");
        return;
    }

    if ((*head)->number == value)
    {
        struct Node *temp = *head;
        *head = (*head)->next;
        free(temp);
        deleteAll(head, value);
        return;
    }

    struct Node *current = *head;
    while (current->next != NULL && current->next->number != value)
    {
        current = current->next;
    }

    if (current->next != NULL)
    {
        struct Node *temp = current->next;
        current->next = current->next->next;
        free(temp);
        deleteAll(&current->next, value);
    }
}
// inserts a new node with the given value after the first node with the given key in the linked list
void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *newNode = createNode(value);

    if (*head == NULL)
    {
        printf("List is empty\n");
        return;
    }

    struct Node *current = *head;
    while (current != NULL &current->number != key)
    {
        current = current->next;
    }

    if (current == NULL)
    {
        printf("Key not found\n");
        return;
    }

    newNode->next = current->next;
    current->next = newNode;
}
//  inserts a new node with the given value after the first node with the given value in the linked list
void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *newNode = createNode(newValue);

    if (*head == NULL)
    {
        printf("List is empty\n");
        return;
    }

    struct Node *current = *head;
    struct Node *previous = NULL;
    while (current != NULL && current->number != searchValue)
    {
        previous = current;
        current = current->next;
    }

    if (current == NULL)
    {
        printf("Value not found\n");
        return;
    }

    newNode->next = current->next;
    previous->next = newNode;
}

